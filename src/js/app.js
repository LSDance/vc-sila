var menuToggler = document.querySelector('.navbar__toggler');
var navbarCollapse = document.querySelector('.navbar__collapse');
menuToggler.addEventListener('click', function (e) {
   document.querySelector('.header').classList.toggle("header--menu-open");
   navbarCollapse.classList.toggle("navbar__collapse--open");
});

var toggleItem = document.querySelectorAll('.toggler');

for (let i = 0; i < toggleItem.length; i++) {
   toggleItem[i].addEventListener('click', function (e) {
      e.preventDefault();
      var target = document.querySelector(toggleItem[i].getAttribute('href'));
      console.log(target)
      target.classList.toggle('toggle-block--open')
      return false;
   })
}

var slide = document.querySelectorAll('.banner-slider__slide');
var rotateTitle = function(index) {
    var slideActive = document.querySelector('.banner-slider__slide--active')
    for (let i = 0; i < slide.length; i++) {
        slide[i].classList.remove('banner-slider__slide--active');
    }
    slide[index].classList.add('banner-slider__slide--active');
};

if (slide.length > 0) {
    var index = 1;
    setInterval(function () {
        rotateTitle(index)
        if(index < 2) {
            index ++;
        }else {
            index = 0;
        }
    },3000)
}

const anchors = [].slice.call(document.querySelectorAll('.anchor')),
    animationTime = 300,
    framesCount = 20;

anchors.forEach(function(item) {
    // каждому якорю присваиваем обработчик события
    item.addEventListener('click', function(e) {
        // убираем стандартное поведение
        e.preventDefault();

        // для каждого якоря берем соответствующий ему элемент и определяем его координату Y
        let coordY = document.querySelector(item.getAttribute('href')).getBoundingClientRect().top + window.pageYOffset;

        // запускаем интервал, в котором
        let scroller = setInterval(function() {
            // считаем на сколько скроллить за 1 такт
            let scrollBy = coordY / framesCount;

            // если к-во пикселей для скролла за 1 такт больше расстояния до элемента
            // и дно страницы не достигнуто
            if(scrollBy > window.pageYOffset - coordY && window.innerHeight + window.pageYOffset < document.body.offsetHeight) {
                // то скроллим на к-во пикселей, которое соответствует одному такту
                window.scrollBy(0, scrollBy);
            } else {
                // иначе добираемся до элемента и выходим из интервала
                window.scrollTo(0, coordY);
                clearInterval(scroller);
            }
            // время интервала равняется частному от времени анимации и к-ва кадров
        }, animationTime / framesCount);
    });
});

var showModalBtns = document.querySelectorAll('.show-modal');
var closeModalBtns = document.querySelectorAll('.close-modal');
var modalOverlay = document.querySelector('.modal-overlay');

modalOverlay.addEventListener('click', function (e) {
   if (e.target == modalOverlay) {
       modalOverlay.classList.remove('modal-overlay--open');
       var modals = document.querySelectorAll('.modal');
       for (let j = 0; j < modals.length; j++) {
           modals[j].classList.remove('modal--open')
       }
   }
});

for (let i = 0; i < showModalBtns.length; i++) {
    showModalBtns[i].addEventListener('click', function (e) {
        e.preventDefault();

        var overlay = document.querySelector('.modal-overlay');
        overlay.classList.add('modal-overlay--open');
        var modal = document.querySelector(this.getAttribute('href'));
        modal.classList.add('modal--open')

        return false;
    })
}

for (let i = 0; i < closeModalBtns.length; i++) {
    closeModalBtns[i].addEventListener('click', function (e) {
        e.preventDefault();

        modalOverlay.classList.remove('modal-overlay--open');
        var modals = document.querySelectorAll('.modal');
        for (let j = 0; j < modals.length; j++) {
            modals[j].classList.remove('modal--open')
        }

        return false;
    })
}

var projectItems = document.querySelectorAll('.projects__item');

for (let i = 0; i < projectItems.length; i++) {
    projectItems[i].addEventListener('click', function (e) {
        // e.preventDefault();
        var projectText = projectItems[i].querySelector('.projects__text');
        var projectTexts = document.querySelectorAll('.projects__text');


        if (projectText.style.opacity == 1) {
            projectText.style.opacity = '0';
            for (let j = 0; j < projectTexts.length; j++) {
                projectTexts[j].style.opacity = '0';
            }
        } else {
            for (let j = 0; j < projectTexts.length; j++) {
                projectTexts[j].style.opacity = '0';
            }
            projectText.style.opacity = '1';
        }
        return false;
    })
}

var ajaxForm = document.querySelectorAll('.ajax-form');

for (let i = 0; i < ajaxForm.length; i++) {
    ajaxForm[i].addEventListener('submit', function (e) {
        e.preventDefault();

        var xhr = new XMLHttpRequest();

        var formData = new FormData(ajaxForm[i]);
        var email = formData.get('email')
        var name = formData.get('name')
        var phone = formData.get('phone')



        var body = 'name=' + encodeURIComponent(name) +
            '&email=' + encodeURIComponent(email)+
            '&phone=' + encodeURIComponent(phone);


        xhr.open('POST', 'mail.php', true);

        xhr.send(formData);

        xhr.onreadystatechange = function() {
            if (this.readyState != 4) return;

            var res = JSON.parse(xhr.responseText);

            console.log(res);

            if (res.status == 'ok') {
                for (let j = 0; j < closeModalBtns.length; j++) {
                    closeModalBtns[i].click()
                }
                alert('Отправлено')
            }else {
                alert('Что то пошло не так')
            }
            // получить результат из this.responseText или this.responseXML
        };

        return false;
    })
}
