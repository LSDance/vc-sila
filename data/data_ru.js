module.exports = {
    lang: 'ru',
    langInt: 'ru',
    meta: {
        title: 'VC Sila',
        description: 'VC Sila',
        keywords: 'VC Sila'
    },
    nav: [
        {
            title: 'портфолио',
            link: '#portfolio',
            name: 'index',
            anchor: false
        },
        {
            title: 'для инвесторов',
            link: 'invest.html',
            name: 'invest',
            anchor: false,
            submenu: [
                {
                    title: 'почему мы',
                    link: '#block1',
                    anchor: true

                },
                {
                    title: 'будущие проекты',
                    link: '#block2',
                    anchor: true
                },
                {
                    title: 'как инвестировать',
                    link: '#block3',
                    anchor: true
                }
            ]
        },
        {
            title: 'для стартапов',
            link: 'startups.html',
            name: 'startups',
            submenu: [
                {
                    title: 'о нас',
                    link: '#block1',
                    anchor: true,
                },
                {
                    title: 'участники',
                    link: '#block2',
                    anchor: true,
                },
                {
                    title: 'программа',
                    link: '#block3',
                    anchor: true,
                },
                {
                    title: 'критерии',
                    link: '#block4',
                    anchor: true,
                }
            ]
        },
        {
            title: 'гранты и субсидии',
            link: 'grants.html',
            name: 'grants',
            submenu: [
                {
                    title: 'Деятельность',
                    link: '#block1',
                    anchor: true,
                },
                {
                    title: 'Этапы работ по грантам',
                    link: '#block2',
                    anchor: true,
                },
                {
                    title: 'Сквозные технологии',
                    link: '#block3',
                    anchor: true,
                },
                {
                    title: 'Государственная поддержка',
                    link: '#block4',
                    anchor: true,
                },
                {
                    title: 'Контакты',
                    link: '#contacts',
                    anchor: true,
                }
            ]
        },
        {
            title: 'контакты',
            link: '#contacts',
            name: 'contacts',
            anchor: true,
        },
    ],
    sectionContacts: {
        address: ' г. Москва, ул. Люсиновская 36,<br>с.1, БЦ "ГЛАСС ХАУС"',
        phone: '+7 (495) 104–39–89',
        email: 'contact@vcsila.com'
    },

    // Главная [ START ]
    sectionBannerIndex: {
        title: 'Мы отбираем,<span class="br--mobile"></span>\n' +
            '                развиваем и вкладываемся<br>\n' +
            '                в инновационные проекты,<span class="br--mobile"></span>\n' +
            '                которые становятся <span class="br--desctop"></span>\n' +
            '                лидерами<span class="br--mobile"></span>\n' +
            '                в мире передовых технологий.',
        list: [
            {
                num: '5',
                desc: 'млн $ ',
                text: '<nobr>объем реализованных</nobr><br>инвестиций'
            },
            // {
            //     num: '15',
            //     desc: 'млн $ ',
            //     text: 'финансовые <span class="br--desctop"></span>показатели'
            // },
            // {
            //     num: '100',
            //     desc: '',
            //     text: 'инвесторов <span class="br--desctop"></span>и фондов'
            // },
            {
                num: '50',
                desc: 'млн $',
                text: 'под управлением'
            },
        ]
    },

    sectionVenture: {
        title: 'Венчурная компания<span class="br--mobile"></span>\n' +
            '                        VC Sila <span class="br--desctop"></span>\n' +
            '                        (Venture Capital<span class="br--mobile"></span>\n' +
            '                        Invest).',
        text_1: ' Мы работаем с фондами,<span class="br--mobile"></span>\n' +
            '                        корпорациями, банками<span class="br--mobile"></span>\n' +
            '                        и частными <span class="br--desctop"></span>\n' +
            '                        инвесторами,<span class="br--mobile"></span>\n' +
            '                        которые инвестируют в самые<span class="br--mobile"></span>\n' +
            '                        перспективные <span class="br--desctop"></span>\n' +
            '                        проекты мира.',
        text_2: 'Мы работаем с начинающими<span class="br--mobile"></span>\n' +
            '                        и развивающимися стартапами<br>\n' +
            '                        в различных отраслях IT<span class="br--mobile"></span>\n' +
            '                        индустрии. Наша компания<br>\n' +
            '                        представляет топовых игроков –<span class="br--mobile"></span>\n' +
            '                        стартапов IT–индустрии <span class="br--desctop"></span>и лучших<span class="br--mobile"></span>\n' +
            '                        выпускников акселераторов.'
    },

    sectionAdvantages: {
        title: 'Наши <span class="br--desctop"></span>преимущества.',
        list: [
            {
                title: 'Надежность',
                text: 'В основе нашей работы<span class="br--mobile"></span>\n' +
                    '                        прозрачные <span class="br--desctop"></span>\n' +
                    '                        условия<span class="br--mobile"></span>\n' +
                    '                        сотрудничества, грамотное <br>\n' +
                    '                        управление рисками и активный <br>\n' +
                    '                        подход к управлению <span class="br--mobile"></span>\n' +
                    '                        инвестициями.',
                img: 'a1a'
            },
            {
                title: 'Партнёрство',
                text: 'Мы признаем только<span class="br--mobile"></span>\n' +
                    '                        взаимовыгодное <span class="br--desctop"></span>\n' +
                    '                        сотрудничество.<span class="br--mobile"></span>\n' +
                    '                        Ценим эффективность <span class="br--desctop"></span>\n' +
                    '                        командной<span class="br--mobile"></span>\n' +
                    '                        работы.',
                img: 'a2'
            },
            {
                title: 'Репутация',
                text: 'Компания дорожит именем<br>\n' +
                    '                        и репутацией, которые заслужила<br>\n' +
                    '                        за годы упорной работы.',
                img: 'a3'
            },
            {
                title: 'Ориентация <span class="br--mobile"></span>на рынок',
                text: 'Подбор инновационных<span class="br--mobile"></span>\n' +
                    '                        проектов <span class="br--desctop"></span>\n' +
                    '                        осуществляется исходя<span class="br--mobile"></span>\n' +
                    '                        из запросов <span class="br--desctop"></span>\n' +
                    '                        и потребностей рынка.',
                img: 'a4'
            },
            {
                title: 'Развитие',
                text: 'Совершенствование бизнес<span class="br--mobile"></span>\n' +
                    '                        процессов <span class="br--desctop"></span>\n' +
                    '                        позволяет повышать<span class="br--mobile"></span>\n' +
                    '                        качество <span class="br--desctop"></span>\n' +
                    '                        предоставляемых услуг.',
                img: 'a5'
            }
        ]
    },

    sectionProjects: {
        title: 'Наши проекты.',
        list: [
            {
                img: 'p1',
                text: 'Российская компания,<span class="br--mobile"></span>\n' +
                    '                        предоставляющая сервис<span class="br--mobile"></span>\n' +
                    '                        на основе технологии WiFi.<span class="br--mobile"></span>\n' +
                    '                        Программное обеспечение<span class="br--mobile"></span>\n' +
                    '                        для авторизации в общественных<span class="br--mobile"></span>\n' +
                    '                        WiFi-сетях, WiFi-сканеры, системы<span class="br--mobile"></span>\n' +
                    '                        идентификации пользователей,<span class="br--mobile"></span>\n' +
                    '                        WiFi-сканеры и многое другое.',
                link: 'https://radiuswifi.ru/'
            },
            {
                img: 'p6',
                text: 'Разработка, интеграция<span class="br--mobile"></span>\n' +
                    '                        и поддержка комплексных IT-решений<span class="br--mobile"></span>\n' +
                    '                        для эффективного развития бизнеса.<span class="br--mobile"></span>\n' +
                    '                        Создают комфортные IT-платформы<span class="br--mobile"></span>\n' +
                    '                        для максимально удобной<span class="br--mobile"></span>\n' +
                    '                        работы.',
                link: 'https://jack-it.ru/'
            },

            {
                img: 'p4',
                text: 'Международная IT компания, занимающаяся разработкой мультифункционального мессенджера с интегрированным функционалом социальных и медиа сетей, нативной цифровой валютой, разработкой собственной платежной системы на основе технологии блокчейн с большим дополнительным функционалом.',
                link: 'https://vinci.id/'
            },
            {
                img: 'p5',
                text: 'Первая глобальная сеть Wi-Fi, <span class="br--mobile"></span>\n' +
                    '                        которая объединяет рекламодателей,<span class="br--mobile"></span>\n' +
                    '                        провайдеров и владельцев частных <span class="br--mobile"></span>\n' +
                    '                        роутеров. Позволяет зарабатывать <br />\n' +
                    '                        на Wi-Fi',
                link: 'https://worldwifi.com'
            },
            {
                img: 'p4',
                text: 'Vinci - первый мессенджер, помогающий держать свою частную жизнь в тайне. В Vinci больше возможностей для общения, чем в традиционных средствах связи: Доступ к скрытым чатам возможен благодаря Секретному паролю. А ввод Подменного пароля имитирует открытие всей переписки.',
                link: 'https://vinci.my/'
            },
            {
                img: 'p7',
                text: 'Инструмент, позволяющий создать и провести конференцию, обучающую программу или презентацию в' +
                    ' виртуальной реальности за 5 минут',
                link: 'http://showmevr.online/'
            },
            {
                img: 'p8',
                text: 'Мультиэкранная ОТТ/IPTV платформа для вещания и просмотра ТВ-каналов с лояльной аудиторией в 10,000,000 пользователей по всему миру.\n',
                link: 'https://vintera.tv/'
            },
            {
                img: 'p9',
                text: 'Лидирующая электронная торговая площадка. Крупнейшая система электронных закупок коммерческих компаний в России. 17 лет стабильной работы',
                link: 'https://b2b-center.ru'
            }
        ]
    },

    sectionInvest: {
        title: 'Инвесторам.',
        text: 'Мы предлагаем фондам,<span class="br--mobile"></span>\n' +
            '                корпорациям,банкам, частным<br>\n' +
            '                инвесторам участие в масштабных<span class="br--mobile"></span>\n' +
            '                инновационных проектах. <br>\n' +
            '                C нашим опытом в области<span class="br--mobile"></span>\n' +
            '                венчурного финансирования,<br>\n' +
            '                отбора и выпуска успешных <span class="br--mobile"></span>\n' +
            '                проектов и вашим участием <br>\n' +
            '                мы сможем внести большой<span class="br--mobile"></span>\n' +
            '                вклад в развитие этого мира.',
        more: 'ПОДРОБНЕЕ'
    },

    sectionStartups: {
        title: 'Для стартапов.',
        text: 'Наша команда состоит из<span class="br--mobile"></span>\n' +
            '                опытных предпринимателей<br>\n' +
            '                и бизнес-профессионалов, которые<span class="br--mobile"></span>\n' +
            '                создали, продали, <span class="br--desctop"></span>\n' +
            '                инвестировали <span class="br--mobile"></span>\n' +
            '                и управляли многочисленными <br>\n' +
            '                технологическими предприятиями, <span class="br--mobile"></span>\n' +
            '                с десятками миллионов <br>\n' +
            '                пользователей и сотнями <span class="br--mobile"></span>\n' +
            '                миллионов в общем доходе. <br>\n' +
            '                С нами вы получите финансовую <span class="br--mobile"></span>\n' +
            '                и менторскую поддержку. <br>\n' +
            '                Мы знаем, как сделать ваш бизнес <span class="br--mobile"></span>\n' +
            '                успешным и устойчивым.',
        more: 'ПОДРОБНЕЕ'
    },
    // Главная [ END ]

    // Инвесторы [ START ]
    sectionBannerInvest: {
        title: 'Венчурный рынок<span class="br--mobile"></span>\n' +
            '                для частных инвесторов. <br>\n' +
            '                Удобный и надежный способ<span class="br--mobile"></span>\n' +
            '                инвестирования <span class="br--desctop"></span>\n' +
            '                в IT стартапы<span class="br--mobile"></span>\n' +
            '                мирового уровня.',
        list: [
            // {
            //     num: '5',
            //     desc: '',
            //     text: 'успешных <span class="br--desctop"></span>выпускников'
            // },
            {
                num: '15',
                desc: '',
                text: 'новых <span class="br--desctop"></span>стартапов'
            },
            {
                num: '50',
                desc: 'млн $',
                text: 'под управлением'
            }
        ]
    },

    sectionInvestInfo: {
        title: 'Инвестиции<span class="br--mobile"></span>\n' +
            '                        в архитектуру <span class="br--desctop"></span>\n' +
            '                        бизнесов<span class="br--mobile"></span>\n' +
            '                        технологических<br>\n' +
            '                        компаний начинаются<span class="br--mobile"></span>\n' +
            '                        от <span class="br--desctop"></span>\n' +
            '                        5 млн рублей.',
        text_1: 'Мы оказываем консалтинговые <span class="br--mobile"></span>\n' +
            '                        услуги для корпораций <span class="br--desctop"></span>\n' +
            '                        по подбору <span class="br--mobile"></span>\n' +
            '                        технологических проектов<span class="br--mobile"></span>\n' +
            '                        и содействуем стартапам<br>\n' +
            '                        в привлечении инвестиций.<span class="br--mobile"></span>\n' +
            '                        Создание компаний осуществляется<br>\n' +
            '                        с целью их продажи корпорациям,<span class="br--mobile"></span>\n' +
            '                        частным предпринимателям<br>\n' +
            '                        и фондам.',
        text_2: 'Благодаря VC Sila,  стал возможен <span class="br--mobile"></span>\n' +
            '                        трансфер и локализация <span class="br--desctop"></span>\n' +
            '                        передовых <span class="br--mobile"></span>\n' +
            '                        международных IT-решений <span class="br--mobile"></span>\n' +
            '                        в Россию. Цифровизация <span class="br--desctop"></span>\n' +
            '                        процессов <span class="br--mobile"></span>\n' +
            '                        разработки и производства IT-<span class="br--mobile"></span>\n' +
            '                        продуктов позволяет <span class="br--desctop"></span>\n' +
            '                        современным <span class="br--mobile"></span>\n' +
            '                        предпринимателям строить <span class="br--mobile"></span>\n' +
            '                        продуктовые компании <span class="br--mobile"></span>\n' +
            '                        быстрее и дешевле.<span class="br--mobile"></span>\n' +
            '                        Инвестиции в архитектуру <span class="br--mobile"></span>\n' +
            '                        бизнесов технологических <span class="br--mobile"></span>\n' +
            '                        компаний начинаются <span class="br--mobile"></span>\n' +
            '                        от 5 млн. рублей.'

    },

    sectionCase: {
        title: 'Мы поможем Вам<br>\n' +
            '                        разработать <span class="br--mobile"></span>\n' +
            '                        портфель<br>\n' +
            '                        инвестиций на ранней <span class="br--mobile"></span>\n' +
            '                        стадии.',
        text: 'Инвестиции в стартапы<span class="br--mobile"></span>\n' +
            '                        предполагают огромный <span class="br--desctop"></span>\n' +
            '                        потенциал<span class="br--mobile"></span>\n' +
            '                        роста, но ими может быть сложно <span class="br--mobile"></span>\n' +
            '                        управлять, <span class="br--desctop"></span>\n' +
            '                        и они недоступны для <span class="br--mobile"></span>\n' +
            '                        большинства индивидуальных<span class="br--mobile"></span>\n' +
            '                        инвесторов. <span class="br--desctop"></span>\n' +
            '                        VC Sila специализируется <span class="br--mobile"></span>\n' +
            '                        на раннем инвестировании.'
    },

    sectionFeaturesProjects: {
        title: 'Будущие проекты.',
        text: 'Список технологических проектов, <span class="br--mobile"></span>\n' +
            '                        в которых участвует <span class="br--desctop"></span>\n' +
            '                        VC Sila, открыт <span class="br--mobile"></span>\n' +
            '                        лишь ограниченному числу <br>\n' +
            '                        венчурных инвесторов.',
        link: 'ЗАПРОСИТЬ СПИСОК'
    },

    sectionHowInvest: {
        title: 'Как инвестировать.',
        list: [
            {
                num: '01',
                text: 'Зайти в раздел <a href="#contacts" class="anchor">контакты</a>\n' +
                    '                        <br>и <a href="tel:+7(495)104–39–89">позвонить</a>\n' +
                    '                        или <a href="mailto:contact@vcsila.com">написать</a> нам\n' +
                    '                        <br>письмо на почту. И наши\n' +
                    '                        <span class="br--mobile"></span>\n' +
                    '                        менеджеры <span class="br--desctop"></span>\n' +
                    '                        свяжутся с Вами.',
            },
            {
                num: '02',
                text: 'Следить за инвестиционными<br>\n' +
                    '                        предложениями, получая<br>\n' +
                    '                        индивидуальную рассылку.<br>',
                link: 'ПОДПИСАТЬСЯ НА РАССЫЛКУ'
            },
            {
                num: '03',
                text: 'Получить подробную консультацию<br>\n' +
                    '                        по заинтересовавшим проектам<br>\n' +
                    '                        у персонального менеджера.<br>',
                link: 'заказать звонок'
            },
            {
                num: '04',
                text: 'Оформить сделку.',
            }
        ]
    },
    // Инвесторы [ END ]

    // Стартапы [ START ]
    sectionBannerStartups: {
        title: [
            {
                title: 'Акселерационная<span class="br--mobile"></span>\n' +
                    '                        программа для стартапов,<span class="br--mobile"></span>\n' +
                    '                        где молодые предприниматели<span class="br--mobile"></span>\n' +
                    '                        в ограниченный промежуток<span class="br--mobile"></span>\n' +
                    '                        времени смогут получить<span class="br--mobile"></span>\n' +
                    '                        поддержку в освоении новых<span class="br--mobile"></span>\n' +
                    '                        рынков напрямую от менторов<span class="br--mobile"></span>\n' +
                    '                        и экспертов в своей отрасли.'
            },
            {
                title: 'Привлечение<span class="br--mobile"></span>\n' +
                    '                        инвестиций под стартапы<span class="br--mobile"></span>\n' +
                    '                        через собственные<span class="br--mobile"></span>\n' +
                    '                        венчурные фонды<span class="br--mobile"></span>\n' +
                    '                        и частные инвестиции.'
            },
            {
                title: 'Масштабные<span class="br--mobile"></span>\n' +
                    '                        проекты с корпорациями<span class="br--mobile"></span>\n' +
                    '                        и государством по созданию<span class="br--mobile"></span>\n' +
                    '                        инновационной среды<span class="br--mobile"></span>\n' +
                    '                        в мире.'
            }
        ],
        list: [
            {
                num: '5',
                desc: 'млн $ ',
                text: '<nobr>объем реализованных</nobr><br>инвестиций'
            },
            {
                num: '20',
                desc: '',
                text: 'менторов <span class="br--desctop"></span>и экспертов'
            },
            {
                num: '80',
                desc: '',
                text: 'партнеров'
            },
            {
                num: '100',
                desc: '',
                text: 'инвесторов <span class="br--desctop"></span>и фондов'
            },
        ]
    },

    sectionAccelerator: {
        title: 'Акселератор VC Sila<br>\n' +
            '                        был учрежден в 2017 году<br>\n' +
            '                        партнерством частных<br>\n' +
            '                        предпринимателей.',
        text: 'Акселератор Sila — это программа <span class="br--mobile"></span>\n' +
            '                        для технологических <span class="br--desctop"></span>стартапов,\n' +
            '                        <span class="br--mobile"></span>\n' +
            '                        основанная на экспертизе и опыте <span class="br--mobile"></span>\n' +
            '                        опытных <span class="br--desctop"></span>\n' +
            '                        предпринимателей <span class="br--mobile"></span>\n' +
            '                        и бизнес-профессионалов, которые<br>\n' +
            '                        создали, продали, инвестировали <span class="br--mobile"></span>\n' +
            '                        и управляли <span class="br--desctop"></span>\n' +
            '                        многочисленными <span class="br--mobile"></span>\n' +
            '                        технологическими предприятиями<br>\n' +
            '                        в мире венчурного и IT-бизнеса. <span class="br--mobile"></span>\n' +
            '                        В течение программы <span class="br--desctop"></span>\n' +
            '                        акселерации <span class="br--mobile"></span>\n' +
            '                        наши эксперты будут помогать вам <span class="br--mobile"></span>\n' +
            '                        развивать <span class="br--desctop"></span>\n' +
            '                        продукт и оттачивать <span class="br--mobile"></span>\n' +
            '                        позиционирование.'
    },

    sectionWyg: {
        title: ' Что вы <span class="br--desctop"></span>получите.',
        list: [
            {
                img: 'wyg1',
                title: 'Менторство',
                text: 'Лучшие эксперты отрасли,<br>\n' +
                    '                        специализирующиеся на <br>\n' +
                    '                        маркетинге, бизнесе и продажах<br>\n' +
                    '                        привлекаются для оценки<br>\n' +
                    '                        и помощи в развитии проекта.'
            },
            {
                img: 'wyg2',
                title: 'Финансирование',
                text: 'Команды с хорошими<br>\n' +
                    '                        промежуточными результатами<br>\n' +
                    '                        получат первоначальный вклад<br>\n' +
                    '                        в компанию. Сумму вклада<span class="br--mobile"></span>\n' +
                    '                        определяют <span class="br--desctop"></span>\n' +
                    '                        эксперты после <span class="br--mobile"></span>\n' +
                    '                        анализа проекта.'
            },
            {
                img: 'wyg3',
                title: 'Сообщество',
                text: 'Обмен опытом с участниками<br>\n' +
                    '                        и выпускниками акселератора,<br>\n' +
                    '                        нетворкинг на наших мероприятиях,<br>\n' +
                    '                        помощь с поиском сотрудников<br>\n' +
                    '                        и партнеров.'
            },
            {
                img: 'wyg4',
                title: 'Инвестиции',
                text: 'Финансирование – ключевой<span class="br--mobile"></span>\n' +
                    '                        компонент <span class="br--desctop"></span>\n' +
                    '                        успеха стартапов <span class="br--mobile"></span>\n' +
                    '                        и один из основных <span class="br--desctop"></span>\n' +
                    '                        направлений <span class="br--mobile"></span>\n' +
                    '                        деятельности VC Capital.<br>\n' +
                    '                        Победители акселератора <span class="br--mobile"></span>\n' +
                    '                        получают <span class="br--desctop"></span>\n' +
                    '                        инвестиции <span class="br--mobile"></span>\n' +
                    '                        на развитие бизнеса.'
            },
            {
                img: 'wyg5',
                title: 'Продвижение ',
                text: 'VC Capital так же занимается<br>\n' +
                    '                        продвижением стартапов <span class="br--mobile"></span>\n' +
                    '                        на российский <span class="br--desctop"></span>\n' +
                    '                        и международный <span class="br--mobile"></span>\n' +
                    '                        рынки путем участия <span class="br--desctop"></span>\n' +
                    '                        в различных <span class="br--mobile"></span>\n' +
                    '                        профильных мероприятиях<br>\n' +
                    '                        и PR стратегии.'
            }
        ]
    },

    sectionProgram: {
        title: 'Программа.',
        list: [
            {
                num: '01',
                title: 'Сбор заявок',
                text: 'Поиск проектов, которые<br>\n' +
                    '                        соответствуют направлениям <br>\n' +
                    '                        и стадиям развития.'
            },
            {
                num: '02',
                title: 'Отбор проектов',
                text: 'Оценка и выбор<br>\n' +
                    '                        проектов, которые попадут<br>\n' +
                    '                        в акселератор.'
            },
            {
                num: '03',
                title: 'Акселерация',
                text: 'Каждую неделю наши эксперты<br>\n' +
                    '                        присоединяются к программе<br>\n' +
                    '                        для проработки различных тем <br>\n' +
                    '                        позиционирования и стратегии <br>\n' +
                    '                        развития бизнеса, а также <br>\n' +
                    '                        индивидуальных запросов <br>\n' +
                    '                        стартапов. ',
                link: 'ПОДРОБНЕЕ',
                more: [
                    {
                        text: 'Участие в лекциях, обучающихворкшопах\n' +
                            '                                и личных консультациях,\n' +
                            '                                встречи с потенциальными клиентами'
                    },
                    {
                        text: 'По оценкам промежуточных результатов \n' +
                            '                                выделяется сумма на упаковку и запуск\n' +
                            '                                пилотной версии продукта'
                    },
                    {
                        text: 'Знакомство и обсуждение возможности \n' +
                            '                                сотрудничества с потенциальнымизаказчиками\n' +
                            '                                на ваш продукт со стороныкомпаний\n' +
                            '                                экосистемы SILA и корпораций – партнеров'
                    },
                    {
                        text: 'Неделя подготовки к питчам дляпроведения\n' +
                            '                                грамотной инвестиционнойпрезентации'
                    }
                ]
            },
            {
                num: '04',
                title: 'Демо день',
                text: 'Презентация результатов<br>\n' +
                    '                        перед выпускной комиссией <br>\n' +
                    '                        акселератора SILA, состоящей <br>\n' +
                    '                        из экспертов компании,<br>\n' +
                    '                        представителей компаний <br>\n' +
                    '                        партнеров, инвесторов <br>\n' +
                    '                        и бизнес-ангелов.',
                link: 'ПОДРОБНЕЕ',
                more: [
                    {
                        text: 'Определение победителейакселератора.'
                    },
                    {
                        text: 'Победители акселератора получат \n' +
                            '                                дополнительные инвестиции на развитие \n' +
                            '                                бизнеса и возможность развития \n' +
                            '                                на российском или международном \n' +
                            '                                рынках.'
                    }
                ]
            },
        ]
    },

    sectionAction: {
        title: 'Если ваш проект <span class="br--mobile"></span>\n' +
            '                        относится <span class="br--desctop"></span>\n' +
            '                        к одной <span class="br--mobile"></span>\n' +
            '                        из этих отраслей,<br>\n' +
            '                        то оставьте заявку<span class="br--mobile"></span>\n' +
            '                        на участие.',
        text: 'Биотехнологии , блокчейн,<span class="br--mobile"></span>\n' +
            '                        онлайн обучение, искусственный<br>\n' +
            '                        интеллект, интернет бизнес,<span class="br--mobile"></span>\n' +
            '                        медицина телекоммуникации,<br>\n' +
            '                        развлечения, техподдержка <span class="br--mobile"></span>\n' +
            '                        автомобили, облачные решения,<br>\n' +
            '                        логистика, Big Data и прочие <span class="br--mobile"></span>\n' +
            '                        технологии.'
    },

    sectionCritery: {
        title: 'Критерии отбора <span class="br--mobile"></span>стартапов.',
        list: [
            {
                num: '01.',
                text: 'Команды из России,<span class="br--mobile"></span>\n' +
                    '                        стран <span class="br--desctop"></span>\n' +
                    '                        СНГ и Балтии.'
            },
            {
                num: '02.',
                text: 'Разработан MVP, есть первые <br>\n' +
                    '                        продажи и клиенты.'
            },
            {
                num: '03.',
                text: 'Ценность продукта подтверждена <br>\n' +
                    '                        реальными клиентами.'
            },
            {
                num: '04.',
                text: ''
            }
        ]
    },
    // Стартапы [ END ]

    btn: {
        getBrif: 'ПОЛУЧИТЬ БРИФ',
        setBrif: 'ЗАПОЛНИТЬ БРИФ',
        send: 'ОТПРАВИТЬ',
        more: 'ПОДРОБНЕЕ',
        subnmitApp: 'ПОДАТЬ ЗАЯВКУ',
    },

    form: {
        titleCallback: 'Заказать обратный звонок',
        titleList: 'Запросить список',
        titleSubscribe: 'Подписаться на рассылку',
        inputPhone: 'Телефон',
        inputEmail: 'Email',
        inputName: 'Имя',
        inputFio: 'ФИО',
    }
};
