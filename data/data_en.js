module.exports = {
    lang: 'en',
    langInt: 'en',
    meta: {
        title: 'VC Sila',
        description: 'VC Sila',
        keywords: 'VC Sila',
    },
    nav: [
        {
            title: 'portfolio',
            link: '#portfolio',
            name: 'index',
            anchor: false
        },
        {
            title: 'for investors',
            link: 'invest.html',
            name: 'invest',
            anchor: false,
            submenu: [
                {
                    title: 'why us',
                    link: '#block1',
                    anchor: true

                },
                {
                    title: 'prospective projects',
                    link: '#block2',
                    anchor: true
                },
                {
                    title: 'how to invest',
                    alink: '#block3',
                    anchor: true
                }
            ]
        },
        {
            title: 'for startups',
            link: 'startups.html',
            name: 'startups',
            submenu: [
                {
                    title: 'about us',
                    link: '#block1',
                    anchor: true,
                },
                {
                    title: 'members',
                    link: '#block2',
                    anchor: true,
                },
                {
                    title: 'programme',
                    alink: '#block3',
                    anchor: true,
                },
                {
                    title: 'criteria',
                    link: '#block4',
                    anchor: true,
                }
            ]
        },
        {
            title: 'contacts',
            link: '#contacts',
            name: 'contacts',
            anchor: true,
        },
    ],
    sectionContacts: {
        address: 'GLASS HOUSE Business Center, 36 Lyusinovskaya street bld 1, <br>Moscow',
        phone: '+7 (495) 104–39–89',
        email: 'contact@vcsila.com'
    },

    // Home page [ START ]
    sectionBannerIndex: {
        title: 'We select, <span class="br--mobile"></span>' +
            '                develop and invest in <br>' +
            '                innovative projects <span class="br--mobile"></span>' +
            '               to make them <span class="br--desctop"></span>' +
            '                top ones <span class="br--mobile"></span>' +
            '                in the high tech world.',
        list: [
            {
                num: '5',
                desc: '$ mln',
                text: '<nobr>total investments</nobr> <br>spent'
            },
            // {
            //     num: '15',
            //     desc: '$ mln',
            //     text: 'financial<span class="br--desctop"></indicators'
            // },
            // {
            //     num: '100',
            //     desc: '',
            //     text: 'investors<span class="br--desctop"></span>and funds'
            // },
            {
                num: '50',
                desc: '$ mln',
                text: 'under control'
            },
        ]
    },

    sectionVenture: {
        title: '’VC Sila <span class="br--mobile"></span>' +
            '                            (Venture Capital<span class="br--desctop"></span>' +
            '                     Invest) <span class="br--mobile"></span>' +
            '                        Venture Сompany',
        text_1: 'We work with funds,  <span class="br--mobile"></span>' +
            'corporations, banks,  <span class="br--mobile"></span>' +
            '                        and private <span class="br--desctop"></span>' +
            '                        investors <span class="br--mobile"></span>' +
            '                        that invest in the most  <span class="br--mobile"></span>' +
            '                        promising <span class="br--desctop"></span>' +
            '                        projects worldwide.',
        text_2: 'We work with both new  <span class="br--mobile"></span>' +
            '                        and advancing startups  <br>' +
            '                       throughout IT  <span class="br--mobile"></span>' +
            '                        industry. Our company <br>' +
            '                       represents only top players  –  <span class="br--mobile"></span>' +
            '                        IT industry startups <span class="br--desctop"></span>and the best  <span class="br--mobile"></span>' +
            '                        graduates of startup accelerators.'
    },

    sectionAdvantages: {
        title: 'Our <span class="br--desctop"></span>advantages.',
        list: [
            {
                title: 'Reliability',
                text: 'Transparent <span class="br--mobile"></span>' +
                    '                        business arrangements,<span class="br--desctop"></span>' +
                    '                    proper risk management <span class="br--mobile"></span>' +
                    '                        and proactive <br>' +
                    '                        investment management philosophy<br>' +
                    '                        lie in the core of  <span class="br--mobile"></span>' +
                    '                        our activities.',
                img: 'a1',
            },
            {
                title: 'Partnership',
                text: 'Mutual benefit <span class="br--mobile"></span>' +
                    '                         is the only acceptable<span class="br--desctop"></span>' +
                    '                       cooperation principle for us.<span class="br--mobile"></span>' +
                    '                        We appreciate efficiency <span class="br--desctop"></span>' +
                    '                        of team <span class="br--mobile"></span>' +
                    '                        work.',
                img: 'a2'
            },
            {
                title: 'Goodwill',
                text: 'Our company ofvalues its good name <br>' +
                    '                        and goodwill established <br>' +
                    '                       throughout the years of hard work.',
                img: 'a3',
            },
            {
                title: 'Market <span class="br--mobile"></span>focus',
                text: 'Innovative <span class="br--mobile"></span>' +
                    '                        projects <span class="br--desctop"></span>' +
                    '                        are selected <span class="br--mobile"></span>' +
                    '                       based on market desires<span class="br--desctop"></span>' +
                    '                        and demands.',
                img: 'a4'
            },
            {
                title: 'Development',
                text: 'Development of business <span class="br--mobile"></span>' +
                    '                        processes<span class="br--desctop"></span>' +
                    '                        allows us to improve <span class="br--mobile"></span>' +
                    '                        the quality<span class="br--desctop"></span>' +
                    '                        of services we provide.',
                img: 'a5',
            }
        ]
    },

    sectionProjects: {
        title: 'Our projects.',
        list: [
            {
                img: 'p1',
                text: 'A Russian company<span class="br--mobile"></span>' +
                    '                       providinge WiFi-based <span class="br--mobile"></span>' +
                    '                        services. <span class="br--mobile"></span>' +
                    '                        Software <span class="br--mobile"></span>' +
                    '                       for public WiFi authorization, <span class="br--mobile"></span>' +
                    '                        WiFi scanners, user identification<span class="br--mobile"></span>' +
                    '                        systems, <span class="br--mobile"></span>' +
                    '                        and a lot more.',
                link: 'https://radiuswifi.ru/'
            },
            {
                img: 'p6',
                text: 'Development, integration, <span class="br--mobile"></span>' +
                    '                       and support for complex IT solutions <span class="br--mobile"></span>' +
                    '                        to successfully develop business. <span class="br--mobile"></span>' +
                    '                        They create user-friendly IT platforms <span class="br--mobile"></span>' +
                    '                        for the most convenient <span class="br--mobile"></span>' +
                    '                        use.',
                link: 'https://jack-it.ru/',
            },

            {
                img: 'p4',
                text: 'Vinci is an international IT company and the developer of Vinci Messenger,' +
                    'a secure and versatile messaging application for  social networks.',
                link: 'https://vinci.id/'
            },
            {
                img: 'p5',
                text: 'The first global Wi-Fi network <span class="br--mobile"></span>' +
                    '                        to bridge advertisers, <span class="br--mobile"></span>' +
                    '                        providers and private <span class="br--mobile"></span>' +
                    '                         routers owners. It allows makeing money <span class="br--mobile"></span>' +
                    '                        on Wi-Fi',
                link: 'https://ru.worldwifi.io/'
            },
            {
                img: 'p4',
                text: 'Vinci is the first messenger, that keeps your private life private. Vinci has more communication options than traditional messengers:\n' +
                    '✔️Secret passcode let to hide and protect separate chats;\n' +
                    '✔️Spoofing passcodes let to imitate an access to all chats!',
                link: 'https://vinci.my/'
            },
            {
                img: 'p7',
                text: 'A tool to prepare and hold a virtual conference, a training program or a slide show just in' +
                    ' five minutes',
                link: 'http://showmevr.ru/'
            },
            {
                img: 'p8',
                text: 'Multi-screen OTT / IPTV platform for broadcasting and watching TV channels with a loyal audience of 10,000,000 users worldwide.',
                link: 'https://vintera.tv/'
            },
            {
                img: 'p9',
                text: 'The leading electronic trading platform. The largest e-procurement system for commercial' +
                    ' companies in Russia. 17 years of stable work',
                link: 'https://b2b-center.ru'
            }
        ]
    },

    sectionInvest: {
        title: 'For investors.',
        text: 'We invite funds, corporations, <span class="br--mobile"></span>' +
            '                banks, and individual <br>' +
            '                investors to take part in <span class="br--mobile"></span>' +
            '                large-scale innovation projects. <br>' +
            '                By combining your participation with our expertise in ' +
            '                venture financing, selection and launch of successful ' +
            '                projects we can substantially contribute to this world’s progress.',
        more: 'MORE'
    },

    sectionStartups: {
        title: 'For startups.',
        text: 'Skilled entrepreneurs and business professionals <br>' +
            '  who have created, sold, invested in, and managed numerous tech enterprises, with tens of millions of users and hundreds of millions of revenues. You receive financial and instructional support from us. <br>' +
            ' We know how to make your business<span class="br--mobile"></span>' +
            ' successful and strong.',
        more: 'MORE'
    },
    // Home page [ END ]

    // Investors [ START ]
    sectionBannerInvest: {
        title: 'Venture market <span class="br--mobile"></span>' +
            '                for individual investors. <br>' +
            '                Easy and secure way<span class="br--mobile"></span>' +
            '                to invest<span class="br--desctop"></span>' +
            '                in world class <span class="br--mobile"></span>' +
            '                IT startups.',
        list: [
            // {
            //     num: '5',
            //     desc: '',
            //     text: 'successful <span class="br--desctop"></span>graduates'
            // },
            {
                num: '15',
                desc: '',
                text: 'New <span class="br--desctop"></span>startups'
            },
            {
                num: '50',
                desc: '$ mln',
                text: 'under control'
            }
        ]
    },

    sectionInvestInfo: {
        title: 'The minimum investment <span class="br--mobile"></span>' +
            '                       in business <span class="br--desctop"></span>' +
            '                        architecture <span class="br--mobile"></span>' +
            '                        of tech <br>' +
            '                        companies <span class="br--mobile"></span>' +
            '                        is <span class="br--desctop"></span>' +
            '                        5 mln rubles.',
        text_1: 'We provide consulting <span class="br--mobile"></span>' +
            '                        services for corporations<span class="br--desctop"></span>' +
            '                        to select <span class="br--mobile"></span>' +
            '                        tech projects <span class="br--mobile"></span>' +
            '                        and assist startups <br>' +
            '                        in attracting investments. <span class="br--mobile"></span>' +
            '                        Companies are established  <br>' +
            '                        to be sold to corporations, <span class="br--mobile"></span>' +
            '                entrepreneurs <br>' +
            '                        and funds.',



            text_2: 'VC Sila is a company organized as a private innovation ecosystem. <span class="br--desctop"></span>' +
            'Under a single umbrella brand SILA brings together startup acceleration programs, investment practice and large-scale private and public projects creating an innovative environment to address modern global challenges.'

    },

    sectionCase: {
        title: 'We assist you to <br>' +
            '                        develop <span class="br--mobile"></span>' +
            '                        an investment <br>' +
            '                        profile <span class="br--mobile"></span>' +
            '                        at early stages.',
        text: 'Investing in startups <span class="br--mobile"></span>' +
            '                        means a huge <span class="br--desctop"></span>' +
            '                        growth <span class="br--mobile"></span>' +
            '                        potential, but may be challenging <span class="br--mobile"></span>' +
            '                        to control <span class="br--desctop"></span>' +
            '                       and may be inapproachable for vast majority <span class="br--mobile"></span>' +
            '                        of individual  <span class="br--mobile"></span>' +
            '                        investors.  <span class="br--desctop"></span>' +
            '                        VC Sila is specialized in<span class="br--mobile"></span>' +
            '                        investment at early stages.'
    },

    sectionFeaturesProjects: {
        title: 'Prospective projects.',
        text: 'The list of tech projects <span class="br--mobile"></span>' +
            '                        participated by <span class="br--desctop"></span>' +
            '                        VC Sila is approachable <span class="br--mobile"></span>' +
            '                       for limited number of<br>' +
            '                        venture investors.',
        link: 'REQUEST THE LIST'
    },

    sectionHowInvest: {
        title: 'How to invest.',
        list: [
            {
                num: '01',
                text: 'Enter <a href="#contacts" class=”anchor”>Contacts section <br>' +
                    '                        <br>and <a href=”tel:+7(495)104–39–89”>call</a>' +
                    '                        or <a href=”mailto:contact@vcsila.com”> send us</a>  ' +
                    '                        <br>email. Our ' +
                    '                        <span class="br--mobile"></span>' +
                    '                        managers <span class="br--desctop"></span>' +
                    '                       will contact you.',
            },
            {
                num: '02',
                text: 'Subscribe to follow our  <br>' +
                    '                        investment <br>' +
                    '                        offers. <br>',
                link: 'SUBSCRIBE TO GET OFFERS'
            },
            {
                num: '03',
                text: 'Our personal manager may provide <br>' +
                    '                      detailed advice <br>' +
                    '                        on any projects of your interest. <br>',
                link: 'request call'
            },
            {
                num: '04',
                text: 'Enter into contract',
            }
        ]
    },
    // Investors [ END ]

    // Startups [ START ]
    sectionBannerStartups: {
        title: [
            {
                title: 'Startup acceleration  <span class="br--mobile"></span>' +
                    '                        programme  <span class="br--mobile"></span>' +
                    '                        for young entrepreneurs to <span class="br--mobile"></span>' +
                    '                        get direct mentor and expert assistance in  <span class="br--mobile"></span>' +
                    '                        developing <span class="br--mobile"></span>' +
                    '                        new  <span class="br--mobile"></span>' +
                    '                       markets.  <span class="br--mobile"></span>' +
                    '   '
            },
            {
                title: 'Attracting  <span class="br--mobile"></span>' +
                    '                        investments for startups  <span class="br--mobile"></span>' +
                    '                        through own  <span class="br--mobile"></span>' +
                    '                        venture funds  <span class="br--mobile"></span>' +
                    '                        and investments from individuals.'
            },
            {
                title: 'Large-scale  <span class="br--mobile"></span>' +
                    '                        projects with corporations  <span class="br--mobile"></span>' +
                    '                        and the government to establish <span class="br--mobile"></span>' +
                    '                        innovation environment  <span class="br--mobile"></span>' +
                    '                        worldwide.'
            }
        ],
        list: [
            {
                num: '5',
                desc: '$ mln',
                text: '<nobr>total investments</nobr>  <br>spent'
            },
            {
                num: '20',
                desc: '',
                text: 'of mentors and <span class="br--desctop"></span>experts'
            },
            {
                num: '80',
                desc: '',
                text: 'partners'
            },
            {
                num: '100',
                desc: '',
                text: 'investors <span class="br--desctop"></span>and funds'
            },
        ]
    },

    sectionAccelerator: {
        title: 'VC Sila Accelerator <br>' +
            '                        was founded in 2017 <br>' +
            '                        as private entrepreneur <br>' +
            '                        partnership.',
        text: 'Sila Accelerator is a programme <span class="br--mobile"></span>' +
            '                        for tech <span class="br--desctop"></span>startups' +
            '                        <span class="br--mobile"></span>' +
            '                        based on expertise of  <span class="br--mobile"></span>' +
            '                        experienced <span class="br--desctop"></span>' +
            '                        enterpreneurs<span class="br--mobile"></span>' +
            '                        and business professionals who have <br>' +
            '                established, sold, invested in,<span class="br--mobile"></span>' +
            '                        and controlled <span class="br--desctop"></span>' +
            '                        many  <span class="br--mobile"></span>' +
            '                       tech enterprises <br>' +
            '                       in the sphere of venture and IT business. <span class="br--mobile"></span>' +
            '                        During the acceleration <span class="br--desctop"></span>' +
            '                        our experts will assist you<span class="br--mobile"></span>' +
            '                        in developing your<span class="br--mobile"></span>' +
            '                        product<span class="br--desctop"></span>' +
            '                        and mastering <span class="br--mobile"></span>' +
            '                        its positioning.'
    },

    sectionWyg: {
        title: ' What <span class="br--desctop"></span>you get.',
        list: [
            {
                img: 'wyg1',
                title: 'Mentoring',
                text: 'Best industry specialists <br>' +
                    '                        in marketing <br>' +
                    '                        business and sales <br>' +
                    '                        will be involved to assess <br>' +
                    '                        and help your project grow.'
            },
            {
                img: 'wyg2',
                title: 'Financing',
                text: 'Teams with good <br>' +
                    '                        interim results <br>' +
                    '                       get the initial investment <br>' +
                    '                       in their company. Investment amount <span class="br--mobile"></span>' +
                    '                        is defined <span class="br--desctop"></span>' +
                    '                        by the experts <span class="br--mobile"></span>' +
                    '                        upon project analysis.'
            },
            {
                img: 'wyg3',
                title: 'Community',
                text: 'Expertise exchange with participants <br>' +
                    '                        and graduates of the accelerators.<br>' +
                    '                        networking at our events, <br>' +
                    '                        assistance in headhunting <br>' +
                    '                        and search for partners.',
            },
            {
                img: 'wyg4',
                title: 'Investments',
                text: 'Financing is the key <span class="br--mobile"></span>' +
                    '                        element of<span class="br--desctop"></span>' +
                    '                        startup success<span class="br--mobile"></span>' +
                    '                        and one of the main <span class="br--desctop"></span>' +
                    '                      activity areas <span class="br--mobile"></span>' +
                    '                        of VC Capital. <br>' +
                    '                        Accelerator winners <span class="br--mobile"></span>' +
                    '                        receive <span class="br--desctop"></span>' +
                    '                        investments <span class="br--mobile"></span>' +
                    '                        to develop their business.'
            },
            {
                img: 'wyg5',
                title: 'Promotion ',
                text: 'VC Capital also promotes <br>' +
                    '                        startups <span class="br--mobile"></span>' +
                    '                        on both Russian <span class="br--desctop"></span>' +
                    '                        and international <span class="br--mobile"></span>' +
                    '                        markets by participating <span class="br--desctop"></span>' +
                    '                        in various <span class="br--mobile"></span>' +
                    '                        field-specific events <br>' +
                    '                        and by developing a PR strategy.'
            }
        ]
    },

    sectionProgram: {
        title: 'Programme.',
        list: [
            {
                num: '01',
                title: 'Receiving applications',
                text: 'Search for projects <br>' +
                    '                        which correspond to<br>' +
                    '                        development directions and stages.'
            },
            {
                num: '02',
                title: 'Project selection',
                text: 'Review and selection <br>' +
                    '                        of projects to enter the <br>' +
                    '                        accelerator.'
            },
            {
                num: '03',
                title: 'Acceleration',
                text: 'Every week our experts <br>' +
                    '                        are involved in the programme <br>' +
                    '                        to work out various subjects of <br>' +
                    '                        business positioning and strategy <br>' +
                    '                        and to satisfy the requests <br>' +
                    '                        of specific startups <br>' +
                    '                        . ',
                link: 'MORE',
                more: [
                    {
                        text: 'Attending lectures, educational workshops, ' +
                            '                                and private consultations, ' +
                            '                                meeting potential clients'
                    },
                    {
                        text: 'The amount to pack and launch  ' +
                            '                                the product pilot version' +
                            '                               is based on interim results'
                    },
                    {
                        text: 'Making acquaintances and discussing possible  ' +
                            '                                cooperation with potential clients' +
                            '                                of your projects from SILA ecosystem companies' +
                            '                               and partnering  corporations'
                    },
                    {
                        text: 'Pitch preparation week to hold' +
                            '                               hold smart investment  presentation'
                    }
                ]
            },
            {
                num: '04',
                title: 'Demo day',
                text: 'Demonstration of results <br>' +
                    '                       to the SILA accelerator graduating board <br>' +
                    '                        which comprises <br>' +
                    '                        company experts, <br>' +
                    '                        representatives <br>' +
                    '                        partners, investors <br>' +
                    '                        and business angels.',
                link: 'MORE',
                more: [
                    {
                        text: 'Selecting accelerator winners.'
                    },
                    {
                        text: 'Accelerator winners will receive  ' +
                            '                                additional investments for business development  ' +
                            '                                and the ability to develop  ' +
                            '                               on both Russian and international  ' +
                            '                                markets.'
                    }
                ]
            },
        ]
    },

    sectionAction: {
        title: 'Apply, ff your project falls to <span class="br--mobile"></span>' +
            '                        any of <span class="br--desctop"></span>' +
            '                        these <span class="br--mobile"></span>' +
            '                        industries <br>' +
            '                        <span class="br--mobile"></span>' +
            '                        .',
        text: 'Bio technologies, blockchain, <span class="br--mobile"></span>' +
            '                        online education, Artificial <br>' +
            '                        intelligence, Internet business, <span class="br--mobile"></span>' +
            '                        medicine, telecom, <br>' +
            '                        entertainment, tech support  <span class="br--mobile"></span>' +
            '                        cars, cloud solutions, <br>' +
            '                        logistics, Big Data and other <span class="br--mobile"></span>' +
            '                        technologies.'
    },

    sectionCritery: {
        title: 'The criteria <span class="br--mobile"></span>of startup selection.',
        list: [
            {
                num: '01.',
                text: 'Teams from Russia, <span class="br--mobile"></span>' +
                    '                        CIS  <span class="br--desctop"></span>' +
                    '                        and Baltics.'
            },
            {
                num: '02.',
                text: 'MVP developed, first sales <br>' +
                    '                        and clients.'
            },
            {
                num: '03.',
                text: 'The product’s value is proven by <br>' +
                    '                        real clients.'
            },
            {
                num: '04.',
                text: ''
            }
        ]
    },
    // Startups [ END ]

    btn: {
        getBrif: 'GET THE BRIEF',
        setBrif: 'SET THE BRIEF',
        send: 'SEND',
        more: 'MORE',
        subnmitApp: 'APPLY',
    },

    form: {
        titleCallback: 'Request callback',
        titleList: 'Request the list',
        titleSubscribe: 'Subscribe for the newsletter',
        inputPhone: 'Phone',
        inputEmail: 'Email',
        inputName: 'Name',
        inputFio: 'Full name',
    }
};

